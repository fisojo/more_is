import pandas as pd

df = pd.read_csv('SICK/new_train.csv')

def determine_score(label):
    if 0 <= label <= 3.8:
        return 0
    elif 3.8 < label <= 5.0:
        return 1
    else:
        return None  
    
df['score'] = df['label'].apply(determine_score)

df = df[df['entailment_judgment'] != 'CONTRADICTION']

df.to_csv('SICK/newest_train.csv', index=False)


# from sentence_transformers import SentenceTransformer, util

# model = SentenceTransformer("all-MiniLM-L6-v2")

# df = pd.read_csv('inputs/Text_Similarity_Dataset 2.csv')

# similarity_scores = []
# for index, row in df.iterrows():
#     paragraph1, paragraph2 = row['text1'], row['text2']
#     embedding1 = model.encode(paragraph1, convert_to_tensor=True)
#     embedding2 = model.encode(paragraph2, convert_to_tensor=True)
#     similarity_score = util.pytorch_cos_sim(embedding1, embedding2)
#     similarity_scores.append(round(similarity_score.item()))

# df['label'] = similarity_scores

# df.to_csv('modified_data.csv', index=False)
# Open the text file and read the lines
# with open('SICK/SICK_train.txt', 'r') as file:
#     lines = file.readlines()
# # Split each line into columns
# data = [line.strip().split('  ') for line in lines]  
# import csv

# # Write data to a CSV file
# with open('SICK/new_train.csv', 'w', newline='') as csvfile:
#     writer = csv.writer(csvfile)
#     writer.writerows(data)

